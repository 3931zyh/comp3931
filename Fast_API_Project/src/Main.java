import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        Main main = new Main();
        List<String> list1 = main.traverseTree(args[0]);
        List<String> list2 = main.traverseTree(args[1]);

        double similarity = findSimilarity(list1, list2);
        System.out.printf("Similarity degree: %.0f%%\n", similarity * 100);
    }

    List<String> traverseTree(String file) throws IOException {
        ParseTreeWalker parseTreeWalker = new ParseTreeWalker();
        CBaseListener cBaseListener = new CBaseListener();
        ParseTree parseTree = parse(file);
        parseTreeWalker.walk(cBaseListener, parseTree);
        return cBaseListener.traversedPath;
    }

    ParseTree parse (String file) throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(file));
        CLexer lexer = new CLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CParser cparser = new CParser(tokens);
        ParseTree tree = cparser.compilationUnit();
        return tree;
    }

    public static int levenshteinDistance(List<String> x, List<String> y) {
        int m = x.size();
        int n = y.size();
        int[][] arr = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++) arr[i][0] = i;
        for (int j = 1; j <= n; j++) arr[0][j] = j;

        int tmp;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                tmp = x.get(i - 1).equals(y.get(j - 1)) ? 0: 1;
                arr[i][j] = Integer.min(Integer.min(arr[i - 1][j] + 1, arr[i][j - 1] + 1),arr[i - 1][j - 1] + tmp);
            }
        }
        return arr[m][n];
    }

    public static double findSimilarity(List<String> x, List<String> y) {
        double max_size = Double.max(x.size(), y.size());
        return max_size > 0 ? (max_size - levenshteinDistance(x, y)) / max_size : 1.0;
    }
}