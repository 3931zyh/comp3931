from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
import subprocess

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # You can specify the allowed origins here, or use ["*"] to allow all origins
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def run_command(command):
    result = subprocess.run(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, shell=True
    )
    return result.stdout, result.stderr


command = "echo 'Hello, World!'"
stdout, stderr = run_command(command)


@app.post("/")
async def root(request: Request):
    json_body = await request.json()
    if not json_body:
        return {"message": "Hello World"}
    else:
        # create and save the file to the server
        with open(json_body["filename1"], "w") as f:
            f.write(json_body["string1"])
        with open(json_body["filename2"], "w") as f:
            f.write(json_body["string2"])
        # run the java program
        stdout, stderr = run_command(
            "java -Xms1024M -cp src/antlr-4.12.0-complete.jar:out Main "
            + json_body["filename1"]
            + " "
            + json_body["filename2"]
        )
        print("stdout: ", stdout)
        print("stderr: ", stderr)
        # delete the files
        run_command("rm " + json_body["filename1"])
        run_command("rm " + json_body["filename2"])
        return {"message": stdout.strip(), "error": stderr.strip()}
