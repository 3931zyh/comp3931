import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomePage from './components/HomePage';
import SecondPage from './components/SecondPage';
import StringContext from './components/StringContext';
import { useState } from 'react';


function App() {

    const [strings, setStrings] = useState({ string1: '', string2: '' });

    return (
        <StringContext.Provider value={{ strings, setStrings }}>
        <Router>
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/second" element={<SecondPage />} />
            </Routes>
        </Router>
        </StringContext.Provider>
    );
}

export default App;
