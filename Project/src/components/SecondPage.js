import React, { useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button } from 'antd';
import * as Diff2Html from "diff2html";
import "../styles.css";
import StringContext from './StringContext';

const Diff = require("diff");

function SecondPage() {

    const { strings } = useContext(StringContext);
    useEffect(() => {
        if (strings.message) {
            console.log("Message:", strings.message);
        }
    }, [strings]);

    const navigate = useNavigate();

    const navigateToHomePage = () => {
        navigate('/');
    };

    // generate unified diff patch
    const diff = Diff.createTwoFilesPatch(
        strings.filename1,
        strings.filename2,
        strings.string1,
        strings.string2,
    );

    let outputHtml = Diff2Html.html(diff, {
        inputFormat: "diff",
        showFiles: true,
        matching: "lines",
        outputFormat: "side-by-side"
    });

    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(outputHtml, "text/html");
    const elements = htmlDoc.getElementsByClassName('d2h-diff-tbody');
    const leftLineNumbers = elements[0].getElementsByClassName('d2h-code-side-linenumber')
    const rightLineNumbers = elements[1].getElementsByClassName('d2h-code-side-linenumber')
    const leftLargestLineNumber = leftLineNumbers[leftLineNumbers.length - 1].innerText.trim();
    const rightLargestLineNumber = rightLineNumbers[rightLineNumbers.length - 1].innerText.trim();
    // TODO - calculate red lines on the left side
    const similarLines = elements[0].getElementsByClassName('d2h-code-side-linenumber d2h-cntx').length;
    console.log(similarLines / leftLargestLineNumber)
    const letfPercentage = (similarLines / leftLargestLineNumber * 100).toFixed(2) + "%";
    const rightPercentage = (similarLines / rightLargestLineNumber * 100).toFixed(2) + "%";
    return (
        <div className='App'>
            <h2>{strings.message}</h2>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <div>{strings.filename1}</div>
                <div>{strings.filename2}</div>
            </div>
            <br></br>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <div>{similarLines}/{leftLargestLineNumber} = {letfPercentage}</div>
                <div>{similarLines}/{rightLargestLineNumber} = {rightPercentage}</div>
            </div>
            <div dangerouslySetInnerHTML={{ __html: outputHtml }}></div>
            <Button type="primary" onClick={navigateToHomePage}>Go to Home Page</Button>
        </div>
    );
}

export default SecondPage;
