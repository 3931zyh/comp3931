// StringContext.js
import { createContext } from 'react';

const StringContext = createContext();

export default StringContext;
