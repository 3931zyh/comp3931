import React, { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { Upload, Button, Typography, Space } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import StringContext from './StringContext';
const { Title } = Typography;

function HomePage() {
    const navigate = useNavigate();

    // const navigateToSecondPage = () => {
    //     navigate('/second');
    // };


    const [file1, setFile1] = useState(null);
    const [file2, setFile2] = useState(null);


    const { strings, setStrings } = useContext(StringContext);

    const uploadProps = {
        customRequest: ({ onSuccess }) => {
            setTimeout(() => {
                onSuccess("ok");
            }, 0);
        },
        listType: "picture",
        maxCount: 1,
    };



    const handleUploadChange1 = (info) => {
        const { fileList } = info;
        if (fileList.length > 0) {
            const lastFile = fileList[fileList.length - 1];
            if (info.file.uid === lastFile.uid) {
                if (info.file.status !== 'removed') {
                    setFile1(info.file.originFileObj);
                } else {
                    setFile1(null);
                }
            }
        } else {
            setFile1(null);
        }
    };

    const handleUploadChange2 = (info) => {
        const { fileList } = info;
        if (fileList.length > 0) {
            const lastFile = fileList[fileList.length - 1];
            if (info.file.uid === lastFile.uid) {
                if (info.file.status !== 'removed') {
                    setFile2(info.file.originFileObj);
                } else {
                    setFile2(null);
                }
            }
        } else {
            setFile2(null);
        }
    };

    const readUploadedFile = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.onload = (event) => resolve(event.target.result);
            fileReader.onerror = (error) => reject(error);
            fileReader.readAsText(file);
        });
    };

    const handleSubmit = async () => {
        if (file1 && file2) {
            const file1Content = await readUploadedFile(file1);
            const file2Content = await readUploadedFile(file2);
            console.log(file1['name'])
            console.log('File 1 content:\n', file1Content);
            console.log(file2['name'])
            console.log('File 2 content:\n', file2Content);
            let result = await fetch('http://localhost:80/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "filename1": file1['name'],
                    "filename2": file2['name'],
                    "string1": file1Content,
                    "string2": file2Content,
                }),
            })
            let msg = await result.json();
            console.log(msg['message']);
            await setStrings({ filename1: file1['name'], filename2: file2['name'], string1: file1Content, string2: file2Content, message: msg['message'] })
            navigate('/second');
                
        }

    };





    return (
        <div style={{ height: '100vh', padding: '2rem', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <Title level={2}>Upload Files</Title>
            <Space direction="vertical" size="large" style={{ textAlign: 'center' }}>
                <Space direction="horizontal" size="large">
                    <Upload {...uploadProps} onChange={handleUploadChange1}>
                        <Button icon={<UploadOutlined />}>Upload File 1</Button>
                    </Upload>
                    <Upload {...uploadProps} onChange={handleUploadChange2}>
                        <Button icon={<UploadOutlined />}>Upload File 2</Button>
                    </Upload>
                </Space>
                <Button type="primary" onClick={handleSubmit}> Submit </Button>
            </Space>
        </div>
    );
}

export default HomePage;
